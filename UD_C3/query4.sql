USE videoclub;

SELECT p.Titol as "Título", i. CodiActor, a.Nom as "Nombre"
FROM PELICULA as p INNER JOIN INTERPRETADA as i ON p.CodiPeli = i.CodiPeli
INNER JOIN ACTOR as a ON i.CodiActor = a.CodiActor;