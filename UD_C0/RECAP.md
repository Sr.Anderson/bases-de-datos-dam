# Unidad C0: Recapitulación

Autor: Francisco Saura Herraiz.

Si quieres ver el contenido del 'READMI' accede a: https://gitlab.com/Sr.Anderson/bases-de-datos-dam/-/tree/main/UD_C0

Introducción (sobre el documento o el tema tratado) -- mejor escribirla al final.

## Concepto y origen de las bases de datos
### ¿Qué son las bases de datos?

Una base de datos es una recopilación organizada de información o datos estructurados, que normalmente se almacena de forma electrónica en un sistema informático. Normalmente, una base de datos está controlada por un sistema de gestión de bases de datos (DBMS).

Fuente: https://es.wikipedia.org/wiki/Base_de_datos

### ¿Qué problemas tratan de resolver? Definición de base de datos.

Facilitar el intercambio de datos en una misma red o con redes externas.

Evitar la duplicación de datos.

Fuente: Explicación en clase.

## Sistemas de gestión de bases de datos

### ¿Qué es un sistema de gestión de bases de datos (DBMS)? 

Es un software de sistema para crear y administrar bases de datos. El DBMS proporciona a los usuarios y programadores una forma sistemática de crear, recuperar, actualizar y administrar datos.

Fuente: https://www.computerweekly.com/es/definicion/Sistema-de-gestion-de-bases-de-datos-o-DBMS

### ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

Persistencia.

Acceder al sistema de manera concurrente.

Indipendencia en el método de almacenamiento.

Abstracción.

Seguridad de datos.

Un mecanismo de bloqueo para acceso concurrente.

Un controlador eficiente para equilibrar las necesidades de múltiples aplicaciones que utilizan los mismos datos.

La capacidad de recuperarse rápidamente de accidentes y errores, incluyendo la capacidad de reinicio y la capacidad de recuperación.

Robustas capacidades de integridad de datos.

Registro y auditoría de la actividad.

Acceso simple usando una interfaz de programación de aplicaciones (API) estándar.

Procedimientos uniformes de administración de datos.

Fuente: https://www.computerweekly.com/es/definicion/Sistema-de-gestion-de-bases-de-datos-o-DBMS

## Ejemplos de sistemas de gestión de bases de datos

### ¿Qué DBMS se usan a día de hoy? 
 
MySQL

MariaDB

Microsoft SQL Server

Oracle DBMS

PostgreSQL

MongoDB

Redis

IBM DB2

Elasticsearch

SQLite 



### ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB : No es software libre. Es cliente-servidor.
* IMB Db2: No es software libre. Es cliente-servidor.
* SQLite: Sí, es software libre. No es cliente-servidor.
* MariaDB: Sí, es software libre. Es cliente-servidor.
* SQL Server: No, no es software libre. Es cliente-servidor.
* PostgreSQL: Si, es software libre. Es cliente-servidor.
* mySQL: No, no es software libre. Es cliente-servidor.

Fuente: https://es.wikipedia.org/wiki/Wikipedia

## Modelo cliente-servidor
### ¿Por qué es interesante que el DBMS se encuentre en un servidor?  

    1. Menor coste por transacción: Distribuyendo los procesos en ordenadores basados en microprocesadores, en lugar de sistemas de mayor tamaño, se obtienen unos costes por transacción mucho más reducidos.

    2. Escalabilidad: Por la naturaleza modular de la arquitectura cliente/servidor, las redes se pueden ampliar fácilmente, aumentando el número de nodos o sustituyendo los nodos por otros más potentes. Este proceso de ampliación no requiere la modificación de las aplicaciones.

    3. Menor coste de desarrollo: La disponibilidad de una gran variedad de herramientas de 4ª generación (4GL) para sistemas cliente/servidor, permite la elección de entornos de desarrollo muy productivos y adaptados a las necesidades de cada usuario.

    4. Entornos gráficos de usuario: Nadie pone en duda que la facilidad de uso de las nuevas herramientas de usuario redundan en una informática más productiva para el usuario, y por tanto para la empresa. Muchas aplicaciones cliente soportan ya estos entornos gráficos.

    5. Interoperabilidad con el puesto de trabajo: Con la evolución de las nuevas herramientas basadas en entornos gráficos, la integración de dichas herramientas con los datos (servidor) es mayor y la expectativa de un uso más productivo de la información aumenta proporcionalmente. 

### ¿Qué ventajas tiene desacoplar al DBMS del cliente?

    1. Seguridad: Al separar el DBMS del cliente, se reduce el riesgo de que los datos sensibles sean comprometidos en dispositivos cliente que pueden ser más vulnerables a ataques o pérdidas de datos.

    2. Portabilidad: Los clientes pueden acceder a la base de datos desde diferentes dispositivos y plataformas, ya que no están directamente vinculados al DBMS. Esto proporciona flexibilidad a los usuarios para utilizar la aplicación desde cualquier lugar.

    3. Rendimiento: Al ejecutar el DBMS en un servidor dedicado, se puede optimizar su rendimiento para manejar consultas y transacciones de manera más eficiente, lo que mejora la velocidad de respuesta para los clientes.

### ¿En qué se basa el modelo cliente-servidor?

    En la división de funcionalidades.

    El cliente envía solicitudes al servidor y muestra los resultados al usuario.

    El servidor es el sistema que proporciona los recursos o servicios solicitados por el cliente. En el contexto de una base de datos, el servidor ejecuta el DBMS y gestiona el acceso a los datos.

Fuente: https://es.wikipedia.org/wiki/Cliente-servidor



* __Cliente__: Usuario que se autentifica contra el servidor para hacer la consulta.
* __Servidor__: El sistema que proporciona el resultado de la consulta. 
* __Red__: Conjunto de dispositivos conectados para compartir recursos y comunicarse.
* __Puerto de escucha__: Identificación de un proceso en un sistema que espera recibir información.
* __Petición__: Mensaje enviado por el cliente al servidor solicitando acción o información.
* __Respuesta__: Mensaje enviado por el servidor al cliente en respuesta a una petición, incluyendo la información solicitada o el resultado de la acción.

## SQL
### ¿Qué es SQL? ¿Qué tipo de lenguaje es?
 El lenguaje de consulta estructurada (SQL) es un lenguaje de programación para almacenar y procesar información en una base de datos relacional. Fuente: https://aws.amazon.com/es/what-is/sql/

### Instrucciones de SQL

#### DDL: 
    DDL significa Data Definition Language o Lenguaje de Definición de Datos, en español. Este lenguaje permite definir las tareas de las estructuras que almacenarán los datos.

    CREATE: Utilizado para crear nuevas tablas, campos e índices.
    ALTER: Utilizado para modificar las tablas agregando campos o cambiando la definición de los campos.
    DROP: Empleado para eliminar tablas e índices.
    TRUNCATE: Empleado para eliminar todos los registros de una tabla.
    COMMENT: Utilizado para agregar comentarios al diccionario de datos.
    RENAME: Tal como su nombre lo indica es utilizado para renombrar objetos.

#### DML:
    DML significa Data Manipulation Language o Lenguaje de Manipulación de Datos, en español. Este lenguaje permite realizar diferentes acciones a los datos que se encuentran en una base de datos.
    
    SELECT: Utilizado para consultar registros de la base de datos que satisfagan un criterio determinado.
    INSERT: Utilizado para cargar de datos en la base de datos en una única operación.
    UPDATE: Utilizado para modificar los valores de los campos y registros especificados
    DELETE: Utilizado para eliminar registros de una tabla de una base de datos.


#### DCL
    Permite crear roles, permisos e integridad referencial, así como el control al acceso a la base de datos.

    GRANT: Usado para otorgar privilegios de acceso de usuario a la base de datos.
    REVOKE: Utilizado para retirar privilegios de acceso otorgados con el comando GRANT.


#### TCL
    Permite administrar diferentes transacciones que ocurren dentro de una base de datos.

    COMMIT: Empleado para guardar el trabajo hecho.
    ROLLBACK: Utilizado para deshacer la modificación que hice desde el último COMMIT.


Fuente: https://platzi.com/tutoriales/50-sql-mysql-2016/1564-que-es-ddl-dml-dcl-y-tcl-integridad-referencial/

## Bases de datos relacionales

### ¿Qué es una base de datos relacional? 

    Una base de datos relacional es un tipo de base de datos que almacena y proporciona acceso a puntos de datos relacionados entre sí. Las bases de datos relacionales se basan en el modelo relacional, una forma intuitiva y directa de representar datos en tablas. 

### ¿Qué ventajas tiene?

    El modelo relacional es sencillo pero muy potente, y lo utilizan organizaciones de todos los tipos y tamaños para una gran variedad de aplicaciones con datos. Las bases de datos relacionales se usan para rastrear inventarios, procesar transacciones de comercio electrónico, administrar cantidades enormes y esenciales de información de clientes y mucho más.

### ¿Qué elementos la conforman?

1. Tablas: Las tablas son la estructura principal de una base de datos relacional. Representan conjuntos de datos relacionados que se organizan en filas y columnas. Cada tabla tiene un nombre único y está compuesta por una o más columnas.

2. Columnas: También conocidas como campos o atributos, las columnas representan las categorías de información que se almacenan en una tabla. Cada columna tiene un nombre único y define el tipo de datos que puede contener (texto, números, fechas, etc.).

3. Filas: Las filas, también llamadas registros o tuplas, son instancias individuales de datos almacenados en una tabla. Cada fila contiene valores específicos para cada una de las columnas de la tabla.

4. Claves primarias: Una clave primaria es un atributo o conjunto de atributos que identifica de manera única cada fila en una tabla. Garantiza la integridad de los datos al prevenir la inserción de registros duplicados o nulos.

5. Claves foráneas: Las claves foráneas establecen relaciones entre tablas. Representan atributos en una tabla que hacen referencia a la clave primaria de otra tabla. Estas relaciones son fundamentales para establecer la integridad referencial en la base de datos.

6. Relaciones: Las relaciones definen la conexión lógica entre tablas en la base de datos. Pueden ser de varios tipos, como uno a uno, uno a muchos o muchos a muchos, y se establecen mediante claves primarias y foráneas.

7. Restricciones de integridad: Las restricciones de integridad son reglas que se aplican a los datos almacenados en la base de datos para garantizar su coherencia y precisión. Incluyen restricciones de clave primaria, restricciones de clave foránea, restricciones de unicidad, entre otras.


* __Relación (tabla)__: Es la estructura principal que organiza los datos en filas y columnas. Cada tabla representa un conjunto de entidades del mundo real.

* __Atributo/Campo (columna)__: Son las categorías o características que describen los datos almacenados en una tabla. Cada columna representa un atributo específico de las entidades representadas por la tabla.

* __Registro/Tupla (fila)__: Son las instancias individuales de datos almacenadas en una tabla. Cada fila representa una entidad específica y contiene los valores correspondientes para cada uno de los atributos definidos en las columnas de la tabla.

Fuente: https://www.oracle.com/es/database/what-is-a-relational-database/

Fuente: https://chat.openai.com/c/59807f9f-7ffb-480b-869f-de69dfa15850