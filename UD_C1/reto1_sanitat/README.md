
Hecho por Francisco Saura Herraiz

Si quieres acceder al repositorio y ver todo el contenido, deberás acceder a: https://gitlab.com/Sr.Anderson/bases-de-datos-dam/-/tree/main/UD_C1/reto1_sanitat


## 1. Muestre los hospitales existentes (número, nombre y teléfono).
```sql
USE sanitat;

SELECT HOSPITAL_COD, NOM, TELEFON
FROM HOSPITAL;
```

## 2. Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.

```sql
use sanitat;

SELECT HOSPITAL_COD, NOM, TELEFON

FROM HOSPITAL

WHERE substring(NOM,2, 1) ='A';

```

## 3. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.

```sql
USE sanitat;

SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM

FROM PLANTILLA;
```


## 4. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.

```sql
USE sanitat;

SELECT TORN HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA
WHERE TORN = 'T' OR TORN = 'M';
```

## 5. Muestre a los enfermos nacidos en 1960.

```sql
USE sanitat;

SELECT INSCRIPCIO
FROM MALALT
WHERE YEAR(DATA_NAIX) = 1960;
```

## 6. Muestre a los enfermos nacidos a partir del año 1960.

```sql
USE sanitat;

SELECT INSCRIPCIO
FROM MALALT
WHERE YEAR(DATA_NAIX) >= 1960;
```
