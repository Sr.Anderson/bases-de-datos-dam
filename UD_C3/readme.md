Hecho por Francisco Saura Herraiz

Si quieres acceder al ejercicio y ver todo el contenido, deberás acceder a: https://gitlab.com/Sr.Anderson/bases-de-datos-dam/-/tree/main/UD_C2/videoclub?ref_type=heads

# En esta sección vamos a realizar consultas usando el JOIN sobre la DB de 'videoclub' .


## 1. Lista todas las películas del videoclub junto al nombre de su género.

```sql
USE videoclub;

SELECT p.CodiPeli, p.Titol, g.Descripcio
FROM PELICULA as p INNER JOIN GENERE as g ON p.CodiGenere = g.CodiGenere;
```

## 2. Lista todas las facturas de María.

```sql
USE videoclub;

SELECT f.CodiFactura
FROM FACTURA as f INNER JOIN CLIENT as c ON f.DNI = c.DNI
WHERE c.Nom like 'Maria%';
```
cambiar el campo data por 'Día de la semana':

select FACTURA.*, dayname(FACTURA.DATA) as "Dia de la semana".


 ## 3. Lista las películas junto a su actor principal.

 ```sql
USE videoclub;

SELECT p.Titol, a.Nom
FROM PELICULA as p INNER JOIN ACTOR as a ON p.CodiActor = a.CodiActor;
```


## 4. Lista películas junto a todos los actores que la interpretaron.

```sql
USE videoclub;

SELECT p.Titol as "Título", i. CodiActor, a.Nom as "Nombre"
FROM PELICULA as p INNER JOIN INTERPRETADA as i ON p.CodiPeli = i.CodiPeli
INNER JOIN ACTOR as a ON i.CodiActor = a.CodiActor;
```

## 5. Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.

```sql
USE videoclub;

SELECT p1.CodiPeli as "Código pelicula", p1.Titol as "Título", p2.CodiPeli as "Codigo película dos", p2.SegonaPart as "Título segunda parte"
FROM PELICULA as p1 INNER JOIN PELICULA as p2 ON p1.CodiPeli = p2.CodiPeli;


