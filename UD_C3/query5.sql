USE videoclub;

SELECT p1.CodiPeli as "Código pelicula", p1.Titol as "Título", p2.CodiPeli as "Codigo película dos", p2.SegonaPart as "Título segunda parte"
FROM PELICULA as p1 INNER JOIN PELICULA as p2 ON p1.CodiPeli = p2.CodiPeli;
